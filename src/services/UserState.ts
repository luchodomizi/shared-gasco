import UserService from "../api/User";
import { useUserStore } from "../store/User/User";

class UserState {
  user$ = useUserStore((state) => state.user);
  getUser$ = useUserStore((state) => state.get);
  updateUser$ = useUserStore((state) => state.update);
  updatePoints$ = useUserStore((state) => state.updatePoints);
  cleanUser$ = useUserStore((state) => state.clearUser);

  get() {
    try {
      return this.getUser$();
    } catch (err) {
      console.log(err);
    }
  }

  async update() {
    try {
      const { status, data } = await UserService.getUserInfo();

      if (status && data) {
        this.updateUser$(data);
      }
    } catch (err) {
      console.log(err);
    }
  }

  updatePoints(points: number) {
    try {
      this.updatePoints$(points);
    } catch (err) {
      console.log(err);
    }
  }

  destroy() {
    try {
      this.cleanUser$();
    } catch (err) {
      console.log(err);
    }
  }
}

const userState = new UserState();
export default userState;
