import { getConfigProject } from "../api/index";
import { useConfigSystemStore } from "../store/ConfigSystem/ConfigSystem";

class ConfigSystemState {
  data$ = useConfigSystemStore((state) => state.configSystem);
  dataUpdate$ = useConfigSystemStore((state) => state.update);
  dataDestroy$ = useConfigSystemStore((state) => state.destroy);
  getData$ = useConfigSystemStore((state) => state.get);

  constructor() {
    try {
      this.update();
    } catch (err) {
      console.log(err);
    }
  }

  get() {
    try {
      return this.getData$();
    } catch (err) {
      console.log(err);
    }
  }

  getUrlApi() {
    try {
      if (this.data$) {
        return this.data$.endpoints.urlApi;
      }
      return "";
    } catch (err) {
      console.log(err);
      return "";
    }
  }

  async update() {
    try {
      const configSystem = await getConfigProject();
      if (configSystem) {
        this.dataUpdate$(configSystem);
      }
    } catch (err) {
      console.log(err);
    }
  }

  destroy() {
    try {
      this.dataDestroy$();
    } catch (err) {
      console.log(err);
    }
  }
}

const configSystemState = new ConfigSystemState();
export default configSystemState;
