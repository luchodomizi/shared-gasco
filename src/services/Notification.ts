import { NotificationModel } from "../models/Notification";
import { useNotificationStore } from "../store/Notification/Notification";

class NotificationState {
  data$ = useNotificationStore((state) => state.notifications);
  dataUpdate$ = useNotificationStore((state) => state.update);
  dataDestroy$ = useNotificationStore((state) => state.destroy);
  getData$ = useNotificationStore((state) => state.get);

  get() {
    try {
      return this.getData$();
    } catch (err) {
      console.log(err);
    }
  }

  async update(notifications: NotificationModel[]) {
    try {
      this.dataUpdate$(notifications);
    } catch (err) {
      console.log(err);
    }
  }

  destroy() {
    try {
      this.dataDestroy$();
    } catch (err) {
      console.log(err);
    }
  }
}

const notificationState = new NotificationState();
export default notificationState;
