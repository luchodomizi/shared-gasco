import PubSub from 'pubsub-js';

class CustomPubSub {
  private subscribers: Record<string, any[]> = {};

  subscribe(event: string, callback: (data: any) => void): string {
    if (!this.subscribers[event]) {
      this.subscribers[event] = [];
    }
    const token = PubSub.subscribe(event, (msg: string, data: any) => {
      callback(data);
    });
    this.subscribers[event].push(token);
    return token;
  }

  publish(event: string, data: any): void {
    PubSub.publish(event, data);
  }

  unsubscribe(event: string, token: string): void {
    if (this.subscribers[event]) {
      PubSub.unsubscribe(token);
      this.subscribers[event] = this.subscribers[event].filter(t => t !== token);
    }
  }
}

export default new CustomPubSub();
