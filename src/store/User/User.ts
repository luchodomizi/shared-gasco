import { create } from "zustand";

interface User {
  name: string;
  role: string;
  points: number | string;
  spaId: string | undefined;
}

interface StoreState {
  user: User;

  get: () => User;
  update: (user: User) => void; // El parámetro user es de tipo Partial<User>
  updatePoints: (points: number) => void;
  clearUser: () => void;
}

export const useUserStore = create<StoreState>((set, get) => ({
  user: {
    name: "Carlos",
    role: "",
    points: 0,
    spaId: undefined,
  },

  get() {
    return get().user;
  },

  update: (user) => {
    set({ user: user });
  },

  // Acción para actualizar los puntos del usuario
  updatePoints: (points) => {
    set((state) => ({
      user: {
        ...state.user,
        points: +points,
      },
    }));
  },

  // Acción para limpiar el estado del usuario
  clearUser: () =>
    set({ user: { name: "", role: "", points: 0, spaId: undefined } }),
}));
