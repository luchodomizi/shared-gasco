import { create } from "zustand";

import { VITE_API_URL } from "../../configs/Constants";

export interface ConfigSystem {
  flags: {
    navbar: boolean;
    sidebar: boolean;
  };
  endpoints: {
    urlApi: string;
    urlApiAuth: string;
    urlResources: string;
    urlApiDistributors: string;
  };
}

export interface StoreState {
  configSystem: ConfigSystem;

  get: () => ConfigSystem;
  getUrlApi: () => void;
  update: (configSystem: ConfigSystem) => void;
  destroy: () => void;
}

// Definir el store de Zustand para la configuración del sistema
export const useConfigSystemStore = create<StoreState>((set, get) => ({
  configSystem: {
    flags: {
      navbar: true,
      sidebar: true,
    },
    endpoints: {
      urlApi: VITE_API_URL || "",
      urlApiAuth: "",
      urlResources: "",
      urlApiDistributors: "",
    },
  },

  get() {
    return get().configSystem;
  },

  // Acción para obtener la URL de la API
  getUrlApi: () => {
    const { configSystem } = get();
    return configSystem.endpoints.urlApi || "";
  },

  // Acción para actualizar la configuración del sistema desde el servicio
  update: (configSystem: ConfigSystem) => {
    try {
      set({ configSystem: configSystem });
    } catch (err) {
      console.log(err);
    }
  },

  // Acción para limpiar la configuración del sistema
  destroy: () =>
    set({
      configSystem: {
        flags: {
          navbar: true,
          sidebar: true,
        },
        endpoints: {
          urlApi: VITE_API_URL || "",
          urlApiAuth: "",
          urlResources: "",
          urlApiDistributors: "",
        },
      },
    }),
}));

export default useConfigSystemStore;
