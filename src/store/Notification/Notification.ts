import { create } from "zustand";
import { NotificationModel } from "../../models/Notification";

interface StoreState {
  notifications?: NotificationModel[];
  total?: number;

  get: () => void;
  update: (notifications: NotificationModel[]) => void;
  destroy: () => void;
}

export const useNotificationStore = create<StoreState>((set, get) => ({
  notifications: [],
  total: 0,

  get() {
    return get().notifications;
  },

  update: (notifications) => {
    set({
      notifications: notifications,
      total: notifications.length,
    });
  },

  destroy: () => set({ notifications: [], total: 0 }),
}));
