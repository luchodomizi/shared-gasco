import axios from "axios";
import { VITE_API_URL } from "../configs/Constants";

export const api = axios.create({
  baseURL: VITE_API_URL,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
    Authorization: `Bearer ${localStorage.getItem("token")}`,
    "x-country": "CL",
    "x-commerce": "GASCO",
    "x-business": "ENVASADO",
    "x-system": "CLUBGASCO",
    "x-user": "CLUBGASCO",
  },
});
