import { api } from "./ApiSystems";
import ConfigSystemState from "../services/ConfigSystem";

class UserService {
  async getUserInfo() {
    try {
      const { status, data } = await api({
        baseURL: `${ConfigSystemState.getUrlApi()}/cg-distributor-order-bff/v1.0.0`,
        url: "/user/info",
        method: "GET",
      });

      if (status === 200) {
        const { name, gascoPoints, sapId } = data.data;
        const user = {
          spaId: `${sapId}`,
          name: `${name}`,
          fullName: `${name}`,
          role: "",
          points: `${gascoPoints}`,
        };
        return { status: true, data: user };
      }
      return {
        status: false,
        message: "No fue posible obtener la información del usuario",
      };
    } catch (err) {
      return { status: false, message: (err as Error).message };
    }
  }
}

export default new UserService();
