import axios, { AxiosResponse } from "axios";
import {
  VITE_REACT_APP_PROPERTIES_URL,
  VITE_API_URL,
} from "../configs/Constants";

export type IResponse =
  | {
      status: boolean;
      message: string;
      data?: any;
    }
  | AxiosResponse;

export type IResponsePromise = Promise<IResponse>;

/* const apiProperty = axios.create({
  baseURL: VITE_REACT_APP_PROPERTIES_URL,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
    Authorization: `Bearer ${localStorage.getItem('token')}`,
    'x-country': 'CL',
    'x-commerce': 'GASCO',
    'x-business': 'ENVASADO',
    'x-system': 'CLUBGASCO',
    'x-user': 'CL'
  }
}); */

export const apiTest = axios.create({
  baseURL: VITE_API_URL,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
    Authorization: `Bearer ${localStorage.getItem("token")}`,
    "x-country": "CL",
    "x-commerce": "GASCO",
    "x-business": "ENVASADO",
    "x-system": "CLUBGASCO",
    "x-user": "CL",
  },
});

export const getConfigProject = async () => {
  try {
    const response = await axios.get(VITE_REACT_APP_PROPERTIES_URL || "", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "x-country": "CL",
        "x-commerce": "GASCO",
        "x-business": "ENVASADO",
        "x-system": "CLUBGASCO",
        "x-user": "CL",
      },
    });

    return response.data;
  } catch (error) {
    console.error(error);
    return null;
  }
};
