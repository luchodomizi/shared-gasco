import { api } from "./ApiSystems";
import userState from "../services/UserState";

class PaymentProvider {
  PREFIX_BACKEND_ORDER = "/cg-distributor-order-bff/v1.0.0";

  async getPaymentOptions() {
    try {
      return await api.get(
        `${this.PREFIX_BACKEND_ORDER}/gasco/paymentoptions/${
          userState.get()?.spaId
        }`
      );
    } catch (error: any) {
      return { status: false, message: error?.message, data: undefined };
    }
  }

  async getPaymentConditions() {
    try {
      return await api.get(
        `${this.PREFIX_BACKEND_ORDER}/gasco/paymentconditions/${
          userState.get()?.spaId
        }`
      );
    } catch (error: any) {
      return { status: false, message: error?.message, data: undefined };
    }
  }
}

export default new PaymentProvider();
