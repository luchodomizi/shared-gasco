import { VITE_API_URL } from "../configs/Constants";
import notificationState from "../services/Notification";
import Notification from "../models/Notification";

class NotificationService {
  async handleEventSourceNotification() {
    try {
      const eventSource = new EventSource(`${VITE_API_URL}/notification`);
      eventSource.onmessage = (e: any) => {
        notificationState.update(
          JSON.parse(e.data)?.map((item: any) => new Notification({ ...item }))
        );
      };
    } catch (err) {
      return { status: false, message: (err as Error).message };
    }
  }
}

export default new NotificationService();
