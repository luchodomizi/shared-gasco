import { useConfigSystemStore } from "./store/ConfigSystem/ConfigSystem"
import { useNotificationStore } from "./store/Notification/Notification"
import { useUserStore } from "./store/User/User"

function App() {

  const userState = useUserStore((state) => state.user)
  const configSystemState = useConfigSystemStore((state) => state.configSystem)
  const notifications = useNotificationStore((state) => state.notifications)

  const updatePoints = useUserStore((state) => state.updatePoints)

  return (
    <>
      <h1>ESTADOS</h1>
      <h2>User</h2>
      <p>{JSON.stringify(userState)}</p>
      <h2>Config System</h2>
      <p>{JSON.stringify(configSystemState)}</p>
      <h2>Notifications</h2>
      <p>{JSON.stringify(notifications)}</p>

      <button onClick={() => updatePoints(15)} >Aumentar puntos</button>
    </>
  )
}

export default App
