export enum IDRoutes {
  orderList = 2,
  orderNew = 1
}

export enum NotificationType {
  order = 'orden'
}

export enum NotificationStatus {
  read = 'read',
  unread = 'unread'
}

export enum MethodPaymentEnum {
  'cash' = 'cash',
  'credit' = 'credit',
  'other' = 'other'
}

export enum MethodPaymentCreditEnum {
  'currentAccount' = 14,
  'check' = 15
}

export enum MethodPaymentCashEnum {
  'webpay' = 17,
  'bank' = 18
}

export enum MethodPaymentOtherEnum {
  'manual' = 16
}
