export const {
  VITE_REACT_APP_PROPERTIES_URL,
  VITE_API_URL,
  VITE_API_URL_LOCAL_HOST,
  NODE_ENV,
} = import.meta.env;

function buildPath(route: string): string {
  const base = NODE_ENV === "production" ? "/clubgasco" : "";
  return `${base}${route}`;
}

export const PathRoutes = {
  orderList: buildPath("/#/pedido-lista"),
  orderNew: buildPath("/#/pedido-nuevo"),
};
