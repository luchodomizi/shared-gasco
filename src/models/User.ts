export interface UserModel {
  id?: string | number;
  name?: string;
  spaId?: string;
  firstName?: string;
  lastName?: string;
  fullName?: string;
  role?: string;
  /** puntos gasco */
  points?: number;
}

class User {
  data: UserModel;

  constructor(data: UserModel) {
    this.data = data;
  }

  static format(data: any) {
    return {
      id: data.id,
      firstName: data.firstName,
      lastName: data.lastName,
      fullName: `${data.name}`,
      name: data.name,
      role: data.role,
      points: data.points,
      spaId: data.spaId
    };
  }
}

export default User;
