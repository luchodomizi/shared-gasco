import { NotificationType, NotificationStatus } from "../configs/Enums";

export interface NotificationModel {
  id?: number;
  title?: string;
  description?: string;
  date?: Date;
  status?: NotificationStatus;
  userId?: number;
  type?: NotificationType;
}

class Notification {
  data: NotificationModel;

  constructor(data: NotificationModel) {
    this.data = data;
  }

  static format(data: any): NotificationModel {
    return {
      id: data.id,
      title: data.title,
      description: data.description,
      date: data.date,
      status: data.read,
      type: data.type,
      userId: data.userId,
    };
  }
}

export default Notification;
