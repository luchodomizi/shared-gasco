export interface ConfigSystemModel {
  flags: {
    navbar: boolean;
    sidebar: boolean;
  };
  endpoints: {
    urlApi: string;
    urlApiAuth: string;
    urlResources: string;
    urlApiDistributors: string;
  };
}

class ConfigSystem {
  data: ConfigSystemModel = {
    flags: {
      navbar: true,
      sidebar: true
    },
    endpoints: {
      urlApi: '',
      urlApiAuth: '',
      urlResources: '',
      urlApiDistributors: ''
    }
  };

  get() {
    try {
      return this.data;
    } catch (err) {
      console.log(err);
    }
  }

  update(configSystem: any) {
    try {
      const { settings } = configSystem;
      this.data = {
        flags: {
          navbar: settings.flags.navbar,
          sidebar: settings.flags.sidebar
        },
        endpoints: {
          urlApi: settings.flags.url_api,
          urlApiAuth: settings.flags.url_api_auth,
          urlResources: settings.flags.url_resources,
          urlApiDistributors: settings.flags.url_api_distributors
        }
      };
    } catch (err) {
      console.log(err);
    }
  }

  destroy() {
    try {
      this.data = {
        flags: {
          navbar: true,
          sidebar: true
        },
        endpoints: {
          urlApi: '',
          urlApiAuth: '',
          urlResources: '',
          urlApiDistributors: ''
        }
      };
    } catch (err) {
      console.log(err);
    }
  }
}

export default ConfigSystem;
