#!/bin/bash
while aws amplify list-jobs --app-id $1 --branch-name $2 --query 'jobSummaries[0].status' | grep -F "RUNNING$PENDING"; do
    sleep 5
done

if aws amplify list-jobs --app-id $1 --branch-name $2 --query 'jobSummaries[0].status' | grep -F "FAILED"; then exit 1; fi

